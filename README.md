# Der Kurs ist beendet.
Es hat viel Spaß gemacht, vielen Dank an alle! Wer im nächsten Kurs dabei sein möchte, schreibt am besten eine Mail an Piko.


# Der 16.11.2022 ist der letzte Termin des Bython-Kurses!
Wir werden nochmal über die Hausaufgaben sprechen und darüber, wie Ihr weitermachen könnt. 
Piko würde sich sehr freuen, Euch alle nochmal zu sehen!


# Am 2.11.2022 muss der Kurs leider entfallen
... weil Piko da leider kein zuverlässiges WLAN hat.
Stattdessen gibt es hier wieder ein Video, diesmal in zwei Teilen.  
- die Hausaufgabenbesprechung: https://video.liberta.vip/w/aMYb2zPUtWN82DZ6FKsYWy
- der neue Stoff: https://video.liberta.vip/w/qfALP1hY9ezhzRFRDDxv2X

Ich arbeite immer noch am Sound; er sollte besser geworden sein; **diesmal ist er nur sehr leise**...

**UPDATE**
Andromeda hat sich der Tonprobleme angenommen und eine lautere Version erstellt:  
Link:
https://gigamove.rwth-aachen.de/en/download/5f9b1ca4a29f4535fbe4920f8eb642d5  
Passwort:
7dA7*MI#rnPI^7|WnhN.

Am 2.11. um 19:30 können sich aber trotzdem Leute im BBB treffen und den Stoff besprechen oder plaudern. Die Stunde am 9.11. wird höchstwahrscheinlich wieder normal stattfinden.

# Am 26.10.2022 muss der Kurs leider entfallen
... weil Piko da gerade auf dem Weg zum Polarkreis ist :D
Stattdessen gibt es hier wieder ein Video:  
https://video.liberta.vip/w/wVeajcWRHmy2HGCPRn2C66

Der Sound ist grässlich; falls Ihr einzelne Stellen nicht versteht, schreibt mir gerne eine Mail – ich schreibe dann die unverständlichen Stellen in die Videobeschreibung. 

Musterlösungen mit Kommentaren findet Ihr im Aufgaben-Ordner; genauso die neuen Hausaufgaben. Bei weiteren Fragen bin ich immer per Mail erreichbar.

Am 26.10. um 19:30 können sich aber trotzdem Leute im BBB treffen und den Stoff besprechen oder plaudern.


# Video für 28.09.
Hier:  
https://video.liberta.vip/w/nBBit73XJi87xggRXK62Xu

**Am 28.09. wird Piko keine Stunde halten**
Hausaufgabenbesprechung und Stoff finden sich im Video. Am 28.09. um 19:30 können sich aber trotzdem Leute im BBB treffen und den Stoff besprechen oder plaudern.

Am 29.09. gibt es die Aufgaben hier im Gitlab.


# Pythonkurs B wie Bython
Pythonkurs für absolute Anfänger*innen and terrified beginners ab August 2022

Der Kurs wird von der Wau-Holland-Stiftung gefördert.
![](https://wauland.de/images/logo-blue.svg)

Hier finden sich Aufgaben, Hinweise und weitere Ressourcen für den Python-Kurs für absolute Anfänger*innen von Piko. Der Kurs läuft von August 2022 bis November 2022 und soll die ersten Schritte im Programmieren vermitteln.
Mehr Informationen auf https://www.haecksen.org/

Erster Termin war der 17.08.
