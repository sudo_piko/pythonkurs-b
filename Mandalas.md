# Mandalas

Hier eine Galerie von mit Turtle erzeugten Mandalas:  


![](https://md.ha.si/uploads/5a907b48-e511-4db1-98b7-83a3e7de3091.png)

![](https://md.ha.si/uploads/ed186ed6-5aeb-4b6a-8e6d-8b7bbb93cc18.png)
![](https://md.ha.si/uploads/3d990399-d76b-4010-aa1d-994d6ccef99f.png)
![](https://md.ha.si/uploads/c96d19eb-821b-45d3-8e0f-4e8ce6fa6e34.png)
![](https://md.ha.si/uploads/6d5b9983-4add-4872-b95a-195099b6c3c4.png)
![](https://md.ha.si/uploads/3c4723e5-1bdc-413e-8c9d-eef081cf256c.png)
![](https://md.ha.si/uploads/fb7491ce-36b2-4ca0-a4d1-9f7f61fe501b.png)
![](https://md.ha.si/uploads/85eef9d8-4382-4c83-8678-2d42c14937e6.png)

![](https://md.ha.si/uploads/0ed56f53-903f-4bd7-8f66-b008f53b324d.png)
![](https://md.ha.si/uploads/81f75bed-a3f5-448e-9e35-92414411a21b.jpg)
![](https://md.ha.si/uploads/cab80df6-1e5c-49d0-afae-7c7523208d48.png)
![](https://md.ha.si/uploads/eb099973-9da4-4e86-aeba-4d575b5c5c54.png)

![](https://md.ha.si/uploads/ac978ba6-1aff-4a80-af39-af2781a42ecb.png)
![](https://md.ha.si/uploads/9000e3c8-f65a-421b-908c-244631a0a268.png)
![](https://md.ha.si/uploads/6c2fdd68-b728-45ec-92f6-5120c041635c.png)



![](https://md.ha.si/uploads/cf7b2ac8-ae09-4bed-b5fd-f5e2f73dede7.png)


