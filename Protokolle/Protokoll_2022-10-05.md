# Pikos Python Kurs am 05.10.2022

## Protokoll

Protokoll ab Vorstellung der Knobelaufgabe:

t0 = time() # setzt für t0 die Startzeit des Programms ein
# print(t0)

while True:
   t = t0-time() # berechnet die Zeit, die das Programm seit dem Start
läuft
   # print(int(t))
   welle = sin(t) + 1 # berechnet den zugehörigen Sinuswert und addiert
1 (damit nichts negatives herauskommt)
   # print(welle)
   print(int(welle*10)*"*") # gibt "das 10-fache des Sinus als
Integer"-Sterne aus (int() damit eine ganze Zahl an sternen ausgegeben
wird/*10 damit der unterschied sichbarer wird)
   sleep(0.2) # unterbricht den Code für 0.2 Sekunden


diesmal haben wir nicht alles importiert (mit *, wie sonst bei turtle),
sondern nur gezielt: 
-  from math import sin #hier wird die Sinus-Funktion importiert (sinus
ist eine formel, die eine welle darstellt)
- from time import time, sleep #hier werden eine Zeit- und eine
Sleep-Funktion importiert;
  - sleep(sec) = Anzahl der Sekunden, die der Code gestoppt wird (sorgt
  dafür, dass das Programm kurz pausiert, bevor es wieder ausgeführt wird)
  - time() liefert die Zahl der Sekunden seit Beginn des 01.01.1970 (time
    ist die fortlaufende zeit, sodass sie bei jedem Durchlauf etwas größer
    ist)


For-Schleifen können iterables bearbeiten.
Umfrage: Was ist iterabel?
- Auflistungen allgemein 
- Strings (auch mit 0 oder 1 Buchstaben)
Aber keine Integer (Zahlen) 
(hierfür müssen die Zahlen vorher in ein String "12345" oder in einem
Tupel (1,2,3,4,5) stehen)

Als nächstes: 
wort = "Pflaumenschüssel"
mit print(wort[5:11]) werden alle buchstaben von der 5. bis zur 11.
stelle ausgegeben. 
Der Doppelpunkt teilt die äußeren Grenzen eines Bereichs. 
wort[7] gibt uns nur eine einen einzigen Buchstaben, in diesem Fall den
an siebter Stelle. 
wort[0] gibt uns den Buchstaben an erster Stelle. 

Jeden Buchstaben in einer eigenen Zeile zwei Mal printen?
```python
for x in wort:
   print(2*x)
```


Und wie kann dabei ein Buchstabe ausgelassen werden?
```python
for x in wort:
   if x != "f"#ungleich "f" #der auszulassende Buchstabe
      print(2*x)
```

oder mit einem counter, der an der stelle, an der der Buchstabe
ausgelassen werden soll die if schleife einsetzt. 
oder:
for x in wort[:1] + wort[2:] #keine zahl wird hierbei wie als (maximale
oder minimale) grenze des verfügbaren bereichs verstanden

Aufgabe 1.2 von der Hausaufgabe: Hier war ein Code von Zahlen
vorgegeben. 
Wie hat Piko diesen Code geschrieben, ohne die Ordnungszahlen der
gewünschten Buchstaben nachschlagen zu müssen? 
wort = "Adventskalender"
codes = ()

for buchstabe in wort: 
    zahl = ord(buchstabe) #in zahl ist jetzt irgendein integer, z.B. 97
    zahlenfolge = str(zahl) #in ziffernfolge ist jetzt ein string, z.B.
"97"
    codes = codes + (ziffernfolge,) # , nach Zahlenfolge um Python zu
zeigen, dass es sich um einen (einstelligen) Tupel handelt.
    print(codes) #Jede Ausführung wird einzelt ausgegeben, weil print
nicht eingerückt ist. 

Ergebnisse der abschließenden Umfrage, bzgl Thempo: viel richtiges
Tempo, einige zu langsam... 
Fragerunde im Anschluss :) 




## Pikos Dateien
### Musterlösungen zu den Hausaufgaben
1.1
```python
namen = ("Hermine", "Ursula", "Kim", "Robin", "Alexis", "Ferdinand")
for name in namen:  # Jeden Namen durchgehen
    print("Guten Tag, " + name + "! Schön, dass du da bist!")  # den jeweiligen Namen in einen String einbauen und printen
```
1.2
```python
codes = ('83', '99', '104', '108', '97', '109', '97', '115', '115', '101', '108')
for c in codes:
    print(chr(int(c)))
```
2.1
```python
# Zielflagge: https://en.wikipedia.org/wiki/Transgender_flag#/media/File:Transgender_Pride_flag.svg
# Farben suchen: http://appjar.info/pythonBasics/#colour-map


from turtle import *


farben = ("skyblue", "pink", "white", "pink", "skyblue")

pensize(20)  # stift mit 20 pixeln Dicke

for farbe in farben:
    pencolor(farbe)  # stiftfarbe setzen
    fd(200)  # Einen Strich malen
    back(200)  # wieder zurück
    
    # einen Streifen tiefer ansetzen
    penup()
    right(90)
    fd(20)
    left(90)
    pendown()
```
2.2
```python
from turtle import *

# strichanzahl = 5  # kann ich mir aussuchen
# Oder mit input:
strichanzahl = int(input("Wie lang sollen die Striche sein?"))
strichlaenge = 20

for i in range(strichanzahl):
    fd(strichlaenge)
    penup()
    fd(strichlaenge)
    pendown()
```

2.3 Schachbrett
```python
from turtle import *

feldlaenge = 30

schwarz = True

for zeile in range(8):
    for feld in range(8):
        if schwarz == True:
            fillcolor("black")
        else:
            fillcolor("white")
        begin_fill()
        for seite in range(4):
            fd(feldlaenge)
            rt(90)
        end_fill()
        schwarz = not schwarz
        fd(feldlaenge)
    penup()
    back(feldlaenge * 8)
    rt(90)
    fd(feldlaenge)
    lt(90)
    pendown()
    schwarz = not schwarz
```

# Iterables
Quiz: Was ist Iterabel?
- a ein einbuchstabiger String
- b ein mehrbuchstabiger String
- c boolean Werte (True und False)
- d Tupel
- e Integers
- f Auflistungen allgemein  
Richtig: abdf
Falsch: ce

https://de.wiktionary.org/wiki/iterieren :
    [1] intransitiv, Rhetorik, Stilkunde: etwas im Satz wiederholen
    [2] transitiv, Mathematik, Informatik: dasselbe Rechenverfahren wiederholt anwenden, um sich der Lösung eines Rechenproblems schrittweise anzunähern
    [3] transitiv, Informatik: eine Auflistung schrittweise durchgehen

```python
for zahl in "":
    print("Hallo!")
    print(zahl)
```
=> Auch ein leerer String ist iterabel; es wird null mal ausgeführt.


```python
wort = "Adventskalender"
codes = ()

for buchstabe in wort:
    zahl = ord(buchstabe)  # in zahl ist jetzt irgendein integer, zB 97
    ziffernfolge = str(zahl)  # in ziffernfolge ist jetzt ein string, zB "97"
    codes = codes + (ziffernfolge,) 
    print(codes)
    
```