# Pikos Python Kurs am 12.10.2022

## Protokoll

Unterricht Bython 12.10.22  

Nützlicher Link zu eckigen Klammern:  
https://www.grund-wissen.de/informatik/python/datentypen.html


Indexierung bei tuple:

Tuple ist ein iterable und wird indiziert.
[Start, bis hierhin wird gegangen (wobei STOPP nicht mitgenommen wird), Schrittweite] 

tuple = (1,2,3,4)
a = tuple[1:3] → Wie beginnenn bei Position 1 und enden VOR Position 3.
print(a) → 2,3

range() hingegen ist eine Funktion.

### Hausaufgaben
Beispiele verstehen: siehe Kopie von Tanja  
Wort finden: alles klar  
1000 Pflaumenschüsseln: Lsungen gern hier posten, Piko kommentiwet, https://md.ha.si/zMZwjQkjR9CZLIfaaC2pOg#

Funktion goto(): sagt der turtle, wo sie starten soll (wie Koordinaten)  
end="": Standardmäßig geht Cursor nach print-Ausgabe in die nächste Zeile. End="" verhindert das. Gleichzeitig lässt sich damit noch etwas einfügen, wenn mensch etwas zwischen die Anführungszeichen schreibt (oder Leerzeichen einfügt)  
(siehe Beispiel UIUI von Piko).  

### Tuple und Listen
Wir zählen bei tuple immer nur die Elemente mit den größten Einheiten. Tuple im tuple zählen als jeweils ein Element. Sind in diesen „Untertuplen“ weitere tuple drin, werden sie nicht mitgezählt, da sie im größeren Element enthalten sind.

Wie wir an ein Element im tuple im tuple kommen:
tuple[][] usw. Jede Klammer führt einen tuple tiefer hinein.


### Einschub: Funktionen
int("6") macht str → integer
str(6) maxht integer zu str

3 * "6" → 666, da 3* ein str ausgegeben wird
3 * 6 → 18, da zwei integer multipliziert werden

dingsda:
range(1,8) kann einer Variablen zugeordnet werden. Die Variable hat dann den type range.
Die Variable mit range kann in einen tuple gepackt werden. Und so haben wir dann einen tuple mit Inhalt aller Ziffern innerhalb der range.

So lässt sich auch ein str in ein tuple verwandeln, das alle Buchstaben des str als einzelen elemente enthält.

Tuple zu str packt alles aus dem tuple in einen str.

### Übergang zu Listen

Listen erkennt mensch an eckigen Klammern. Listen können, was tuple können und mehr, sind aber komplizierter.

Tuple (und auch range, siehe oben) können in Listen gewandelt werden: list(tuple)

Listen haben Methoden. Methoden sind Funktionen, die mit einem . Auf Variablen angewendet werden (siehe Beispiel Piko).

list.append → hängt etwas an den Inhalt der Liste dran.
Syntax hierzu → Liste.methode(argument)












## Pads
Plaumenschüssel von letztem Mal:  
https://md.ha.si/zMZwjQkjR9CZLIfaaC2pOg#

Mandalas: (OMG die sind alle so großartig!)  
https://md.ha.si/DUhDhhECQ9-XZwWM5K4t_w#

## Pikos Datei
```python
range(2, 8, 3)
    
    
tupel = (1,2,3,4)
a = tupel[1:3]

print(a)
```

```python
for c in "uiaeonrtdy":
    print(c, end="      ")
```
```python
tupel0 = ("hui", "uiui", "oyoyoyo!")
tupel1 = ("hui", "uiui", (1, 2, 4))
tupel2 = ("hui", "uiui", (1, 2, 4), ("a", "b", "c", (333, 323, 469)))


# Aufgaben
# 
# 1. bei tupel0: an das "uiui" kommen
# 2. bei tupel1: an das "uiui" kommen,
# 3. dann an (1, 2, 4)

a = tupel1[2] # => a = (1, 2, 4)
b = a[0] # => b = 1

die_eins = tupel1[2][0]
#           (1, 2, 4)[0]

# print(die_eins)
# print(tupel1[2][0])  # geht auch ohne Variable dazwischen
# print(tupel1[1:2])  # frage aus dem Chat

print(   tupel2[3]   [3]   [1]   )
print(len(tupel2))
```

```python
int("6") # => 6

str(6) # => "6"


"6"*3
6*3


dingsda = range(1, 8)
print(type(dingsda))

dings2 = tuple(dingsda)
print(type(dings2))
print(dings2)
# 
# print(type(dingsda), dingsda)
# print(type(dings2), dings2)
```


```
>>> s = "hallohallo"
>>> tuple(s)
('h', 'a', 'l', 'l', 'o', 'h', 'a', 'l', 'l', 'o')
>>> tupel4 = (0, 5, 8)
>>> str(tupel4)
'(0, 5, 8)'
>>> list(tupel4)
[0, 5, 8]
>>> liste1 = ["hallo", "apfel", "müsli"]
>>> liste1[1]
'apfel'
>>> liste1.append("tomate")
>>> liste1
['hallo', 'apfel', 'müsli', 'tomate']
>>> liste.methode(argument)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'liste' is not defined
>>> l = list(range(10))
>>> l
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```