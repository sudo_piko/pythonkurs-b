# Pikos Python Kurs am 24.08.2022


## Protokoll aus den geteilten Notizen
0) Gruppen für Nachzügler*innen :)
1) Aufgaben besprechen vom 17.8.
2) Themen heute
* "Argumentenübergabe"
* tuple
* print()
* len()
* max()
* Vergleichsoperatoren: 

    * < 

    * ==

(https://lernenpython.com/operators/)
* Variablen boxen
* float, int


## Protokoll von einer Lernenden

Ergebnis-Protokoll Python-Kurs vom 24.08.22
### Hausaufgaben-Check
Alle Aufgaben und der Lesestoff sind natürlich freiwillig, aber empfehlenswert, um das Verständnis zu vertiefen.
Taschenrechner-Aufgaben:
Leerzeichen bei Addition machen keinen Unterschied im Ergebnis.
Zwei Sterne ( ** ) potenzieren eine Zahl, also 2 ** 3 = 8
Der Befehl „//“ ist eine Ganzzahldivision ohne Rest, also 23//7 = 3
Der Befehl „%“ errechnet den Rest nach einer Ganzzahldivision, also 23%7 = 2

Aufgaben 2 
Wörter werden als „string“, also Zeichenkette vom Programm verstanden.
b = „hallo“
print(b*3)
hallohallohallo
Man kann also sozusagen mit Zeichenketten, also Wörtern, „rechnen“.

Klammern sind bei Funktionen sehr wichtig.
Manche Funktionen benötigen ein Argument, jedoch nicht alle. Argument ist der Inhalt in der Klammer z.B. print(„hallo“)
Es gibt Unterschiede bei dem Ergebnis, wenn der Inhalt der Klammer mit Komma oder Plus getrennt wird, z.B.
```
>>> print ("eins", "zwei")
eins zwei
>>> print("eins" + "zwei")
einszwei
```
Letzteres wird nicht einfach wiedergegeben, sondern addiert.
```
>>> print(("eins", "zwei", "drei"))
('eins', 'zwei', 'drei')
```
=> Hier wird nur ein Element wiedergegeben, was der Inhalt in der inneren Klammer ist.

### Verschiedene Befehle
Der Befehl „len“ zählt die Elemente in der Klammer
```
>>> len(("eins", "zwei", "drei"))
3
```
Der Befehl max sucht den höchsten Wert des Elements heraus, z.B.
```
>>> max(1, 2, 3)
3
>>> max("hallo")
'o'
```
Hier ein Link, der die „Wertigkeit“, nach der die Funktion rechnet, auflistet:
https://www.itwissen.info/en/ANSI-character-set-112827.html#gsc.tab=0

„Variablen“ sind wie Schachteln, in die man Inhalt packt.
„Werte“ sind mögliche Schachtelinhalte.
```
>>> ein_wort = "Schnee"
>>> ein_wort*3
'SchneeSchneeSchnee'
```
Die Funktion type bezieht sich auf Buchstabenketten, auch string genannt.
```
>>> type("3")
<class 'str'>
>>> "3" == 3
False
```
Hier kann man den Unterschied zwischen Wert und der Aneinanderreihung der Zahl 3 sehen. Den Unterschied machen die Anführungszeichen.
```
>>> s= "3"
>>> f = 3
>>> s*3
'333'
>>> f*3
9
>>> type(("eins", "zwei", "drei"))
<class 'tuple'>
```

Als tupel bezeichnet man die Aneinanderreihung in der Klammer.

### Ankündigung: 
Am kommenden Samstag ist der Tag der offenen Türe des „Hackspace“ in vielen Städten. 
https://www.ccc.de/de/updates/2022/offenehackerspaces




## Pikos Kommandozeile
```
Python 3.10.6 (/usr/bin/python)
>>> 456+789
1245
>>> 456 + 789
1245
>>> 456 ** 4
43237380096
>>> 456^4
460
>>> 23//7
3
>>> 23%7
2
>>> 2.0*4
8.0
>>> 2*4
8
>>> 2.0 == 2
True
>>> b = "hallo"
>>> print(b * 3)
hallohallohallo
>>> input("Bitte Namen!")
Bitte Namen!uiaetrn
'uiaetrn'
>>> a = 2,0
>>> a*3
(2, 0, 2, 0, 2, 0)
>>> input("uieand")
uieand
''
>>> c = input("uentdeutrd")
uentdeutrdHallo
>>> input()
uiae
'uiae'
>>> print("eins", "zwei", "drei")
eins zwei drei
>>> print()
einszweidrei
>>> "eins" + "zwei" + "drei"
'einszweidrei'
>>> print(("eins", "zwei", "drei"))
('eins', 'zwei', 'drei')
>>> len(('eins', 'zwei', 'drei'))
3
>>> len('eins', 'zwei', 'drei')
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: len() takes exactly one argument (3 given)
>>> max()
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: max expected at least 1 argument, got 0
>>> max(1)
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
TypeError: 'int' object is not iterable
>>> max("hallo")
'o'
>>> max("Hallo")
'o'
>>> max("hallo", "tschüss")
'tschüss'
>>> max(1, 2, 3)
3
>>> ord("a")
97
>>> ord("A")
65
>>> max("a", "A")
'a'
>>> ord("b")
98
>>> ord("h")
104
>>> ord("t")
116
>>> "h" < "t"
True
>>> "h" > "t"
False
>>> ein_wort = "Schnee"
>>> ein_wort*3
'SchneeSchneeSchnee'
>>> "Schnee"*3
'SchneeSchneeSchnee'
>>> w = ein_wort
>>> w
'Schnee'
>>> ein_wort
'Schnee'
>>> type(w)
<class 'str'>
>>> type("u")
<class 'str'>
>>> type(".")
<class 'str'>
>>> type("")
<class 'str'>
>>> "3" == 3
False
>>> s = "3"
>>> z = 3
>>> z*3
9
>>> s*3
'333'
>>> type(3)
<class 'int'>
>>> type(3.0)
<class 'float'>
>>> type(('eins', 'zwei', 'drei'))
<class 'tuple'>
>>> ("h", "j", "k")
('h', 'j', 'k')
>>> (1, 3, 8)
(1, 3, 8)
>>> (1, 2, "hallo", "Schokolade")
(1, 2, 'hallo', 'Schokolade')
>>> for b in "hallo":
    print(b*2)
    
hh
aa
ll
ll
oo
>>> a
(2, 0)
>>> b
'o'
>>> a == b
False
>>> a = b
>>> a
'o'
>>> 
```