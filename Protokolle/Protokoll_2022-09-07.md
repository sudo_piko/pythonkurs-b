# Pikos Python Kurs am 07.09.2022


Pad zur Namensfindung der Gruppe .. irgendwas mit B ... Vorschläge hier notieren:
https://md.ha.si/avA5mCE0QBuITFR_w2Jl-Q

Hausaufgabennachbesprechung:
https://gitlab.com/sudo_piko/pythonkurs-b/-/blob/main/Aufgaben/Aufgaben_2022-08-31.md


Letzte Aufgabe von 1:
"Neue Aufgabe: Findet die Länge der in eine Zeichenkette umgewandelten negativen Zahl, die in dem String in der Variable hallo gespeichert ist."

Lösung:

```python
print(len(str(-abs(hallo))))
```
Funktions- bzw Operatorenübersicht:
https://calc.aachen.ccc.de/ld5qtayxlus1

!= ungleich

Aufgabe 2.2:
2 + 3 == 3
Welcher Operator wird als erstes ausgeführt? Addition

Exkurs boolean (siehe Pikos Shell)

Aufgabe 3:
Exkurs 'Ausdruck'

name == "Ada"  <- Ausdruck!

---
#eine Verzweigung

```python
if name == "Ada": 
    #wenn der Name == Ada ist
    print("Oah, Ada, ich hab ewig auf dich gewartet!")

else:
    #andernfalls
    print("Ui, dich kenne ich gar nicht.")
```


Aufgabe 4:
```python
zahl1 = input("Gib mir eine Zahl! ")
zahl1 = int(zahl1)
 
zahl2 = input("Gib mir noch eine Zahl! ")
zahl2 = int(zahl2)
 
print("Was ist die Summe dieser Zahlen?")
antwort = input()
antwort = int(antwort)

if antwort == zahl1 + zahl2:
    print("Richtig")

else:
    print("Falsch")
```


NEU: #eine weitere Verzweigung, KEINE Schleife
```python
zahl1 = input("Gib mir eine Zahl! ")
zahl1 = int(zahl1)
 
zahl2 = input("Gib mir noch eine Zahl! ")
zahl2 = int(zahl2)
 
print("Was ist die Summe dieser Zahlen?")
antwort = input()
antwort = int(antwort)

if antwort == zahl1 + zahl2:
    print("Richtig")

else:
    print("Falsch")
    if antwort > zahl1 +zahl2:
        print("Zu hoch gegriffen, versuchs noch mal")
    else:
        print("zu niedrig")
```


Nächste Aufgabe aus https://md.ha.si/avA5mCE0QBuITFR_w2Jl-Q#:
hier kam ich nicht mehr mit, siehe -> Pikos Shell



## Pads
https://md.ha.si/avA5mCE0QBuITFR_w2Jl-Q?view


## Bemerkung
Piko verspricht sich manchmal bei Verzweigungen und sagt, das seien Schleifen. Das stimmt nicht! 
Schleifen ist etwas anderes, das nur so ähnlich aussieht. Da kommen wir noch hin.
Dieses if-Zeugs sind alles Verzweigungen.

## Pikos Dateien
Eine einfache Verzweigung
```python
name = input("Wer bist du?")

# Eine Verzweigung!
if name == "Ada":
    # wenn der name == Ada:
    print("Oah, Ada, ich hab auf dich gewartet!")
else:
    # andernfalls:
    print("Ui, dich kenn ich gar nicht")

print("Hallo!")
```
Mathetrainingsprogramm mit if
```python
zahl1 = input("Gib mir eine Zahl! ")
zahl1 = int(zahl1)

zahl2 = input("Gib mir noch eine Zahl! ")
zahl2 = int(zahl2)


print("Was ist die Summe dieser Zahlen?")
antwort = input()
antwort = int(antwort)

# Das hier ist eine Verzweigung:
if antwort == zahl1 + zahl2:
    print("Richtig!")
else:
    print("Das war falsch!")
```

Mathetrainingsprogramm mit if, mit mehreren Ebenen
```python
zahl1 = input("Gib mir eine Zahl! ")
zahl1 = int(zahl1)

zahl2 = input("Gib mir noch eine Zahl! ")
zahl2 = int(zahl2)


print("Was ist die Summe dieser Zahlen?")
antwort = input()
antwort = int(antwort)

# Das hier ist eine Verzweigung:
if antwort == zahl1 + zahl2:
    print("Richtig!")
else:
    print("Das war falsch!")
    if antwort > zahl1 + zahl2:
        print("Zu hoch gegriffen! Versuch es nochmal!")
        if antwort > zahl1 + zahl2 + 5:
            # zB 12 >  2   +   3    +5
            #         (richtig   )  + 5
            print("Das war viel zu hoch!")
    else:
        print("Zu niedrig! Probier es nochmal!")
```

## Pikos Kommandozeile
```
>>> zahl = 23
>>> abs(23)
23
>>> hallo = "123456"
>>> str(hallo)
'123456'
>>> int(hallo)
123456
>>> hallo2 = int(hallo)
>>> hallo
'123456'
>>> hallo2
123456
>>> hallo2*2
246912
>>> 
>>> hallo*2
'123456123456'
>>> len(str(-abs(hallo2)))
7
>>> len(str(-abs(hallo)))
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: bad operand type for abs(): 'str'
>>> abs(len(str(hallo)))
6
>>> a = 6
>>> b = 5
>>> a == b
False
>>> a == 6
True
>>> a != b
True
>>> a != 6
False
>>> 

2 + 3 == 3


False
>>> 2 + (3 == 3)
3
>>> int(2 + (3 == 3))
3
>>> int(2 + 3 == 3)
0
>>> 2 + True == 3
True
>>> int(2 + True == 3)
1
>>> bool(1)
True
>>> bool(0)
False
>>> bool(2000)
True
>>> bool(0.00000000000001)
True
>>> bool(-1)
True
>>> bool("eins")
True
>>> bool("null")
True
>>> bool("0")
True
>>> bool("")
False
>>> 1 + True
2
>>> bool("False")
True
>>> int("1")
1
>>> bool("0")
True
>>> int("0")
0
>>> bool(0)
False
>>> bool("0")
True
>>> bool(int("0"))
False
>>>  False == ""
False
>>> False == ""
False
>>> bool("")
False
>>> False == 0
True
>>> name = input("?")
?Piko


>>> %Run -c $EDITOR_CONTENT
Wer bist du?Piko
Ui, dich kenn ich gar nicht
Hallo!
>>> "Piko" == "Ada"
False
```