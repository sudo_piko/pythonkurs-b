# Pikos Python Kurs am 02.11,


## Pikos Dateien

### Hausaufgaben

Debugging
```python
# (Version ohne Fehler:)

def begruessung(name1, name2):
    print(name1 + ": Hallo " + name2 + "!")
    print(name2 + ": Hallo " + name1 + "!")
    print(name1 + ": Wie geht es dir?")
    print(name2 + ": Mir geht es gut, danke.")
def wetter(name1, name2, regenwahrscheinlichkeit):
    if regenwahrscheinlichkeit > 0.5:
        print(name1 + ": Ich nehme einen Schirm mit.")
        print(name2 + ": Ich nehme auch einen Schirm mit.")
    else:
        print(name1 + ": Ich nehme keinen Schirm mit.")
        print(name2 + ": Ich nehme auch keinen Schirm mit.")
def sinn_des_lebens(name1, name2, der_sinn):
    print(name1 + ": Was, denkst du, ist der Sinn des Lebens?")
    if der_sinn:
        print(name2 + ": Ich glaube, der Sinn des Lebens ist " + der_sinn + ".")
    else:
        print(name2 + ": Ich weiß es nicht.")
def abschied(name1, name2):
    print(name1 + ": Tschüss.")
    print(name2 + ": Tschüss.")
def gespraech(name1, name2):
    begruessung(name1, name2)
    wetter(name1, name2, 0.7)
    sinn_des_lebens(name1, name2, "42")
    print("(Zeit vergeht.)")
    wetter(name1, name2, 0.2)
    sinn_des_lebens(name1, name2, None)
    abschied(name1, name2)

gespraech("Ada", "Margaret")
```



### Inhalt
```python
def quersumme(zahl):
    ziffern = str(zahl)
    ergebnis = 0
    for ziffer in ziffern:
        ergebnis = ergebnis + int(ziffer)  # in ziffer ist zB "3"
    return ergebnis

print(quersumme(1234))
print(quersumme(1234)+5)

quersumme_1 = quersumme(1234)
print(type(quersumme_1))  # => int



def quersumme_print(zahl):
    ziffern = str(zahl)
    ergebnis = 0
    for ziffer in ziffern:
        ergebnis = ergebnis + int(ziffer)  # in ziffer ist zB "3"
    print(ergebnis)
    
quersumme_2 = quersumme_print(1234)
print(type(quersumme_2))
```


```python
def summe(zahl1, zahl2):
    ergebnis = zahl1 + zahl2
    return ergebnis

def summe_kurz(zahl1, zahl2):
    return zahl1 + zahl2

def quadrieren(zahl):
    ergebnis = zahl * zahl
    # ergebnis = zahl ** 2  # geht auch
    return ergebnis

def quadrieren_kurz(zahl):
    return zahl ** 2

def immer_richtig():
    return True

if immer_richtig():
    print("Das hier wird immer ausgegeben")
```