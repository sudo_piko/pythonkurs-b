# Pikos Python Kurs am 17.08.2022

Präsentation:  
https://md.ha.si/s/h9wZTQe7G#

## Geteilte Notizen im BBB

Test
Meoww

https://www.haecksen.org
https://www.wauland.de/de/

https://senfcall.de/


Fragen: Piko@riseup.net
Aufgaben und Protokolle:
https://gitlab.com/sudo_piko/pythonkurs-b


https://md.ha.si/8yhOjMR0SymS3MPEkInSQg#

Ressourcen links die geteilt wurden:
    https://www.w3schools.com/python/python_operators.asp



## Pikos Kommandozeile
```
Python 3.10.5 (/usr/bin/python)
>>> print("Hallo Welt!")
Hallo Welt!
>>> "Hallo Welt"
'Hallo Welt'
>>> print("Hallo Welt!')
  File "<pyshell>", line 1
    print("Hallo Welt!')
          ^
SyntaxError: unterminated string literal (detected at line 1)
>>> import turtle
>>> import turtl
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
ModuleNotFoundError: No module named 'turtl'
>>> import turtle
>>> 3+8
11
>>> 3*8
24
>>> 9/3
3.0
>>> 10/3
3.3333333333333335
>>> a = 3
>>> "Hallo Welt" == 'Hallo Welt'
True
>>> 3
3
>>> kiste = "teddybär"
>>> kiste
'teddybär'
>>> 3+5
8
>>> %Run Pyschnipsel.py
8
>>> print(3+5)
8
>>> 3+5
8
>>> %Run Pyschnipsel.py
>>> "teddybär'
  File "<pyshell>", line 1
    "teddybär'
    ^
SyntaxError: unterminated string literal (detected at line 1)
>>> "Hallo Welt" == 'Hallo Welt'
True
>>> kiste = "murmel"
>>> kiste
'murmel'
>>> print(kiste)
murmel
>>> kiste2 = "teddybär"
>>> kiste1 + kiste2
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
NameError: name 'kiste1' is not defined
>>> kiste + kiste2
'murmelteddybär'
>>> kiste = kiste2
>>> kiste
'teddybär'
>>> kiste2
'teddybär'
>>> 
>>> kiste1 = "murmel"
>>> kiste
'teddybär'
>>> kiste1
'murmel'
>>> kiste2
'teddybär'
>>> kiste = kiste1 + kiste2
>>> kiste
'murmelteddybär'
>>> kiste = schuetteln(kiste1) + foehnen(waschen(kiste2))
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
NameError: name 'schuetteln' is not defined
>>> kiste = [kiste1, kiste2]
>>> kiste
['murmel', 'teddybär']
>>> 
```