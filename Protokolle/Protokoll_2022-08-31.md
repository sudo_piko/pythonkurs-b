# Pikos Python Kurs am 31.08.2022

## Protokoll

### Umfragen am Anfang

- Gruppeneinteilung: Wer ist versorgt?
	- 31 (91%) Ja
	- 2 (6%) Nein
	- 1 (3%) Enthaltungen
- Wer hat bisherige Aufgaben zur eigenen Zufriedenheit gelöst
	- Ja: 33 (77%)
	- Nein: 6 (14%)
	- Enthalten: 4 (9%)

### Vergleichen der Aufgaben

1. len()
	- "len() zählt die Anzahl der Elemente eines iterierbaren Datentyps"
	- iterierbar = wenn etwas mehrere Teile hat --> ein Brot ist nur ein Stück, aber 25 Brötchen sind iterierbar (man kann also ein bestimmtes davon verwenden)
2. str()
	- für alle klar
3. chr() und obs()
	- Ja: 36 ||||||||||||||||| 82%
	- Nein: 5 ||| 11%
	- Enthaltung: 3 || 7%
	- chr(ord(89)) = 89 --> beide sind genau Umkehrung voneinander
	- chr() = Ascii zahl in character
	- ord() = character zurück in ascii Zahl
	- Lösung war "Schlamassel"
4. abs()
	- wandelt Zahlen immer in positive Zahlen um
	- -abs(zahl) macht immer negative zhale daraus
	- ascii zhalen sind immer positiv --> abs dafür unnötig
	- Umfrage alles klar?
		- Ja: 46 ||||||||||||||||||| 90%
		- Nein: 5 ||| 10%
		- Enthaltung: 0 | 0%
5. len()
	- max(len(wort1), len(wort2), len(wort3)) --> gibt die länge des längsten wortes an
	- max(wort1, wort2, wort3) --> sortiert alphabetisch
6. Aufgabe 4
	- Umfrage Zur eigenen Zufriedenheit erledigt:
		- Ja: 34 |||||||||||||| 68%
		- Nein: 13 |||||| 26%
		- Enthaltung: 3 || 6%
	- Wie liest man code
		- Struktur grob ansehen
		- genauer ansehen: Input, Verarbeitung, Output
		- wo gibt es den gleichen Aufbau ("Syntax")
		- klammern "(zahl1 + zahl2) - antwort" sollen es nur besser lesbar machen
	- variablen dürfen auch direkt überschrieben werden (z.B. zahl1 = int(zahl1))
	- Umfrage Ist es jetzt klar?
		- Ja: 38 |||||||||||||||||| 86%
		- Nein: 1 | 2%
		- Enthaltung: 5 ||| 11%
7. Argument, Element, Wert
	- Argument = Etwas in den Klammern einer Funktion
	- Zählen anfangen immer bei 0
	- Tupel werden verwendet, um mehrere Elemente in einer einzigen Variablen zu speichern.
	- Element = ein Teil einer Aufzählung
	- Werte = Sachen, die eins in einer Variable speichern kann.
8. Gemeinsame Tabelle
	- [Tabelle](https://calc.aachen.ccc.de/ld5qtayxlus1)
9. Operatoren
	- = Zuweiungsoprator
	- == Vergleichsoperator (auch >, <, <=, >=, !=)
	- arithmetische Operatoren: +, -, \*, /
	- "a" < "z" ist so ähnlich wie max("a", "z")
		- 1: 39 ||||||||||||||||| 80%
		- 2: 5 ||| 10%
		- 3: 5 ||| 10%
	- nicht string und int mischen! --> kann man nicht vergleichen!
	- type(False) --> <class 'bool'>
	- Ein Ausdruck ist etwas, wo python etwas "ausrechnen" (oder vergleichen) kann
10. Auswertungsreihenfolge
	- print("Hallo " + name + " !In zwei Jahren wirst du " str(alter + 2) + " alt sein!")
	- beginn ganz innen und dann weiter nach außen (wie in Mathe)
11. Umfrage wie war das Tempo
	- 1: zu schnell: 6 ||| 12%
	- 2: richtig:   36 ||||||||||||||| 71%
	- 3: zu langsam: 4 || 8%
	- 4: Enthaltung: 5 ||| 10%
	
	
	


## Pads
Mindmap/"Tabelle" für die Begriffe:  
https://calc.aachen.ccc.de/ld5qtayxlus1

## Pikos Datei

```
Auswertungsreihenfolge


print("Hallo " + name + "! In zwei Jahren wirst Du " + str(alter + 2) + " sein.") 
                                                            15

print("Hallo " + name + "! In zwei Jahren wirst Du " + str(  15  + 2) + " sein.") 
														        17

print("Hallo " + name + "! In zwei Jahren wirst Du " + str(	   17   )  + " sein.") 
														 "17"

print("Hallo " + name +  "! In zwei Jahren wirst Du " +   "17"         + " sein.") 
               "Robin"

print("Hallo " +"Robin"+ "! In zwei Jahren wirst Du " +   "17"         + " sein.") 
	                "Hallo Robin! In zwei Jahren wirst Du 17 sein."

print("Hallo Robin! In zwei Jahren wirst Du 17 sein.") 
```


## Pikos Kommandozeile
(Hier fehlen die Rückgaben vom Computer, die könnt Ihr aber selbst generieren!)
``` 
chr(89)
>>> ord(chr(89))
>>> chr(ord("Y"))
>>> ord("2")
>>> 2 == "2"
>>> ord(str(2))
>>> chr(ord(str(2)) + 2)
>>> zahl = 5
>>> zahl = -33
>>> abs(zahl)
>>> -abs(zahl)
>>> zahl1 = 6
>>> zahl2 = 0
>>> zahl3 = 1
>>> zahl4 = -32
>>> zahl5 = -5
>>> abs(zahl1)
>>> abs(zahl4)
>>> -1 * abs(zahl)
>>> ord("A")
>>> -1 * ord("a")
>>> buchstabe = "d"
>>> -1 * ord(buchstabe)
>>> wort1 = "euainteuadnr"
>>> wort2 = "eua"
>>> wort3 = "ueandeuartdueu"
>>> max(len(wort1, wort2, wort3))
>>> max(len(wort1), len(wort2), len(wort3))
>>> max(wort1, wort1, wort3)
>>> h = "hallo"
>>> h * 3
```
(Ab hier mit Rückgaben)
```
>>> %Run -c $EDITOR_CONTENT
Gib mir eine Zahl! 2
Gib mir noch eine Zahl! 3
Was ist die Summe dieser Zahlen?
9
Das war falsch! Das war falsch! Das war falsch! Das war falsch! 
>>> ord("a")
97
>>> hallo = "hallihallo"
>>> t = (2,5,8)
>>> # Zuweisungsoperator: =
>>> # Vergleichsoperator: ==
>>> meins = "brombeer"
>>> deins = "schokolade"
>>> meins == deins
False
>>> ihrs = "schokolade"
>>> deins == ihrs
True
>>> 2 + 6
8
>>> 2 + 6 == 8
True
>>> 3 + 5 == 9
False
>>> 3 + 5 === 9
  File "<stdin>", line 1
    3 + 5 === 9
            ^
SyntaxError: invalid syntax
>>> 3 + 5 > 5
True
```