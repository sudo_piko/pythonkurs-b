# Pikos Python Kurs am 16.11.2022

**Letzte Stunde**

## Pads
- Lösungen für Hangman: https://md.ha.si/iCG7eRxJRWuyN2QJg_z2tg#
- Vorschlagsammlung zum Weitermachen: https://md.ha.si/-eKWuguaSwCe0uW5CCjlwA#
- Was Ihr noch gern gelernt hättet: https://md.ha.si/r4TKU1XiQLCVDl45KbMRnA#
- Feedback an Piko: https://md.ha.si/KrrGccWdRTuRFTsLKQt7oQ#
- Weitermach-Ideen für Bython: https://md.ha.si/EmSmMFHcRxqetRf7nIpLFA#


## Pikos Dateien Hausaufgabenbesprechung

### Quiz
```python
aufgabenliste = [("Frage1", "Antwort1"), ("Frage2", "Antwort2"), ("Frage3", "Antwort3")]


def frage_stellen(frage, richtige_antwort):
    gegebene_antwort = input(frage)
    if gegebene_antwort == richtige_antwort:
        return True
    else:
        return False
    
def schluss(name, punkte, punkte_max):
    print("Das Quiz ist zu Ende.")
    print("Du hast", punkte, "von", punkte_max, "möglichen Punkten erreicht.")
    anteil = punkte / punkte_max
    print("Das ist ein Anteil von", anteil)
    if anteil >= 0.7:
        print("Sehr gut,", name)
    else:
        print("Üb doch noch mal,", name)

# ausgabe = frage_stellen("Wer bist du denn?", "Piko")
# 
# print(ausgabe)

punkte = 0
name = input("Wer bist du?")
for paar in aufgabenliste:
    # print(paar)
    frage, richtige_antwort = paar
#     print("Die Frage ist:", frage)
    ergebnis = frage_stellen(frage, richtige_antwort)
    if ergebnis:
        punkte = punkte + 1
        print("Gut!")
    else:
        print("Falsch!")
        
schluss(name, punkte, len(aufgabenliste))
```

### Hangman
```python
# ueberschallknall

loesung = ['u', 'e', 'b', 'e', 'r', 's', 'c', 'h', 'a', 'l', 'l', 'k', 'n', 'a', 'l', 'l']
max_fehler = 7
# _ _ _ _ _ _ _ ...
# _ e _ e _ _ _ 


def ermittle_position(loesung, buchstabe):
    """teste wo buchstabe vorkommt und gib Liste von Indizes zurück"""  # Das ist ein Docstring
    indizes = []
    for i in range(len(loesung)):
        if loesung[i] == buchstabe:
            indizes.append(i)
    return indizes 

def stand_ausgeben(liste):
    for zeichen in liste:
        print(zeichen, end=" ")
    print()

def buchstaben_ersetzen(buchstabe, indizes, anzeigeliste):
    for index in indizes:
        anzeigeliste[index] = buchstabe

# buchstaben_ersetzen("ö", [3, 4, 5, 8], loesung)
# print(loesung)

def anzeigeliste_bauen(wort):
    ergebnisliste = []
    for buchstabe in wort:
        ergebnisliste.append("_")
    return ergebnisliste



fehler = 0

anzeigeliste = anzeigeliste_bauen(loesung)

# while Schleife, _ noch in anzeigeliste
while "_" in anzeigeliste:
    # frage nach buchstabe
    b = input("Buchstabe? ")
    # ermittle_position
    positionen = ermittle_position(loesung, b)
    # anzeigeliste aktuaisieren
    if positionen == []:
        print("Buchstabe kommt nicht vor.")
        fehler = fehler + 1
    else:
        buchstaben_ersetzen(b, positionen, anzeigeliste)
    stand_ausgeben(anzeigeliste)
    
    
# Gratulation
```


## Weitermach-Ideen

- Wo weiterlernen?
    - https://www.practicepython.org/
    - sehr kondensiert: https://anandology.com/python-practice-book/getting-started.html#running-python-interpreter
    - https://www.codingame.com/start
    - https://programminghistorian.org/en/lessons/working-with-text-files
- Aufgaben-Ideen:
    - https://www.python-lernen.de/hangman-python-programmieren.htm
    - https://medium.com/byte-tales/the-classic-tic-tac-toe-game-in-python-3-1427c68b8874
    - Advent of Code: https://adventofcode.com/ – wird schnell recht schwer
    - Projekt Euler: https://projecteuler.net/archives – sehr mathematisch
    - Abraxas-Kurs: https://gitlab.com/sudo_piko/pykurs
- Nächster Kurs:
    - https://gitlab.com/sudo_piko/pythonkurs-c
- Andere Kurse von anderen Anbieties? 
    - Automate the boring stuff: Buch und Videos: https://www.youtube.com/watch?v=1F_OgqRuSdI&list=PL0-84-yl1fUnRuXGFe_F7qSH1LEnn9LkW&index=1
    - https://www.youtube.com/watch?v=jvZLWhkzX-8&list=PLbd_WhypdBbAMyFfKgSj27JO7CEpuIcEK
    - https://open.hpi.de/courses/pythonjunior2020
    - eher gemütlich: https://cscircles.cemc.uwaterloo.ca/de/
- Weiter treffen!