# Pikos Python Kurs am 

## Pad: Mögliche Hausaufgabenlösungen
https://md.ha.si/_lUsp2OaQ2Wbz79yCzypxQ#
mit ein paar Kommentaren von Piko

## Pikos Dateien

### Hausaufgabenbesprechung
```python
alter = 6

if alter > (15 + int("2")):
    print("Du darfst wählen!")
    
if True:
    ...
    
if False:
    ...
    
def kein_grossbuchstabe(string):
    for buchstabe in string:
        if buchstabe in "ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜẞÅ":  # ist ein Großbuchstabe:
#         if buchstabe.isupper():  # ist ein Großbuchstabe:
            return False
    return True
        
s = "halloH!"
if kein_grossbuchstabe(s):
    print("Hier ist kein Großbuchstabe!")
else:
    print("Hier ist ein Großbuchstabe!")
    
    
    
def funktion1():
    print("Hallo!")
    
    return True
```
```python
z1 = 3
z2 = 4

def quadratsumme_print(zahl1, zahl2):
    ergebnis = zahl1**2 + zahl2**2
    print(ergebnis)
#     return ergebnis


def quadratsumme_return(zahl1, zahl2):
    ergebnis = zahl1**2 + zahl2**2
    return ergebnis

q = quadratsumme_print(z1, z2)
print(2*q)
```
```python
buchstabe = "A"

if buchstabe.isupper():
    print("Das ist ein Großbuchstabe!")
    


def is_sum(zahl1, zahl2, vermutete_summe):
    wahre_summe = zahl1 + zahl2
    if wahre_summe == vermutete_summe:
        return True
    else:
        return False


fuenf = 5
dreiundzwanzig = 23
gegebene_antwort = int(input("Was ist 5 + 23?"))

if not is_sum(fuenf, dreiundzwanzig, gegebene_antwort):
    print("Das war die falsche Antwort!")
```
```python
# bla.bla@bla

def is_emailaddress(string):
    if "@" not in string:
        return False
    if "." not in string:
        return False
    liste = string.split("@")
    if len(liste) > 2:
        return False
    name, domain_und_tld = liste
    if "." not in domain_und_tld:
        return False
    if ...
    
    return True

if is_emailaddress("bl?a@bla?h.at") == True:
    print("Das ist eine korrekte Mailadresse.")
else:
    print("Da stimmt was mit der Mailadresse nicht!")
```

### Inhalt: Default-Parameter
```python
from turtle import*

def dreieck1():
    for i in range(3):
        fd(100)
        rt(120)
        
dreieck1()

def dreieck2(laenge):
    for i in range(3):
        fd(laenge)
        rt(120)
        
dreieck2(120)


def dreieck3(laenge=100, farbe="blue"):
    pencolor(farbe)
    for i in range(3):
        fd(laenge)
        rt(120)


def dreieck4(strichdicke, laenge=100, farbe="blue"):
    pencolor(farbe)
    pensize(strichdicke)
    for i in range(3):
#         print(i)
        fd(laenge)
        rt(120)
        
# positional arguments:        
dreieck4(3, 20, "red")

# keyword arguments:       
dreieck4(strichdicke=3, laenge=20, farbe="red")


range(..., 10, ...)
range(5, 10)
range(12, 200, 5)
```


## Musterlösungen zu den Hausaufgaben
### is_emailaddress
```python
def is_emailaddress(string):
    if "@" not in string:
        return False
    if "." not in string:
        return False
    liste = string.split("@")
    if len(liste) > 2:
        return False
    name, domain_und_tld = liste
    if "." not in domain_und_tld:
        return False
    liste = domain_und_tld.split(".")
    if len(liste) > 2:
        return False
    domain, tld = liste
    if (len(name) < 1) or (len(domain) < 1) or (len(tld) < 2):  # Alle Längen ausprobieren. Wenn eins zu kurz ist, ist es kaputt.
        return False
    for zeichen in name:
        if not zeichen.isalnum():  # isalnum: is alpha or numeric, also ein Buchstabe oder eine Zahl
            if zeichen not in "._-":
                return False
    if not domain.isalnum():  # in der Domain nur Buchstaben oder Zahlen.
        return False    
    if not tld.isalpha():  # in der Top-Level-Domain nur Buchstaben.
        return False
    return True


if is_emailaddress("bla@blah.at") == True:
    print("Das ist eine korrekte Mailadresse.")
else:
    print("Da stimmt was mit der Mailadresse nicht!") 
```

### is_palindrome
Erster Schritt. Hier können wir dem "geputzten" string beim Wachsen zusehen:
```python
def is_palindrome1(text):
    text = text.lower()
    string = ""
    for zeichen in text:
        if zeichen.isalpha():
            string += zeichen
        print(string)
        
is_palindrome1("Madam, I'm Adam!")
```
Nächster Schritt, jetzt sehen wir die Buchstaben. 
```python
def is_palindrome1(text):
    text = text.lower()
    string = ""
    for zeichen in text:
        if zeichen.isalpha():
            string += zeichen
#         print(string)
    for i in range(len(string)):
        print(string[i], "und", string[-i])
        
is_palindrome1("Madam, I'm Adam!")
```
Aber etwas klappt noch nicht, der Buchstabe, der nach "und" geprintet wird, ist immer eins hinten dran...
Also müssen wir die print-Zeile etwas verändern:
```python
print(string[i], "und", string[-(i+1)])
```
Dann klappt es. Das jetzt noch aus dem print in einen Vergleich bringen:
```python
def is_palindrome1(text):
    text = text.lower()
    string = ""
    for zeichen in text:
        if zeichen.isalpha():
            string += zeichen
#         print(string)
    for i in range(len(string)):
#         print(string[i], "und", string[-(i+1)])
        if string[i] != string[-(i+1)]:  # != ist "ungleich"
            return False
    return True  # Hier ist es wieder: Wenn wir alle Optionen durchgegangen sind, und keine hat uns False ausgegeben, dann muss es True sein!
```

... und als ganzes Programm:
```python
palindromliste = ["otto", "Anna", "kajak", "radar", "level", "rotor", "kobyla ma maly bok", "Able was I ere I saw Elba", "Madam, I'm Adam", "Was it a car or a cat I saw?"]

def is_palindrome(text):
    text = text.lower()
    string = ""
    for zeichen in text:
        if zeichen.isalpha():
            string += zeichen
    for i in range(len(string)):
        if string[i] != string[-(i+1)]:  # != ist "ungleich"
            return False
    return True  # Hier ist es wieder: Wenn wir alle Optionen durchgegangen sind, und keine hat uns False ausgegeben, dann muss es True sein!

for p in palindromliste:
    if is_palindrome(p):  # Hier findet der Test statt.
        print(p, "ist ein Palindrom.")
    else:
        print(p, "ist kein Palindrom.")
```