# Pikos Python Kurs am 21.09.2022

## Protokoll
1. 28.09.2022 findet der Kurs als Video statt, welches im GitLab unter Hausaufgaben geteilt wird.
2. Unter [tldraw](https://www.tldraw.com/r/1662661671451) wurde die ehemalige Tabelle mit Übersicht von den teilnehmenden überarbeitet.
3. Falls man sich bezüglich den **Boleans** unssicher ist, kann unter [w3school](https://www.w3schools.com/python/python_booleans.asp) das Wissen vertieft werden.
4. Themen des heutigen Abends: if, while, break, Schildkröten laufen lassen...
5. Hausaufgaben Besprechung:
  1. Aufgabe 2 - Partysnacks 2 <br>
  Wieso ist die neue Version besser?
  > Erst die Variablen zu durchdenken, macht den Code kürzer und übersichtlicher.
  <br>  
  > mehr zwischenschritte, macht den code lesbarer
  <br>
  2.  Aufgabe 3 - if und while <br>
  Lösungen aus Aufgabe 3.3 (Matheübungsprogramm)

<details>
<summary>Lösugen</summary>

  ```python
  zahl1 = input("Gib mir eine Zahl! ")
  zahl1 = int(zahl1)

  zahl2 = input("Gib mir noch eine Zahl!")
  zahl2 = int(zahl2)

  print("Was ist die Summe dieser Zahlen?")
  antwort = input()
  antwort = int(antwort)

  ergebnis_falsch = (zahl1 + zahl2) != antwort
  n_versuche = 1

  while ergebnis_falsch:
      n_versuche = n_versuche + 1
      print("Das war falsch! Probiere es nochmal. ", n_versuche, ".Versuch: ")
      antwort = int (input("Was ist die Summe dieser Zahlen? "))
      if (zahl1 + zahl2) == antwort:
          ergebnis_falsch = False

  print("Richtig. Im", str(n_versuche)+ ". Versuch gelöst!")
  ```

  ```python
  #Rechenprogramm 2
  i_versuch = 0

  zahl1 = int(input("Gib mir eine Zahl! "))
  zahl2 = int(input("Gib mir noch eine Zahl! "))
  ergebnis = (zahl1 + zahl2)

  while True:
      print("Was ist die Summe dieser Zahlen?")
      antwort = int(input())

      if ergebnis != antwort :
          print ('Das war falsch!')
          i_versuch = i_versuch + 1
      else:
          break

  if i_versuch == 1:
      print ('Herzlichen Glückwunsch! Du hast', i_versuch, 'Versuch gebraucht!')
  else:
      print ('Herzlichen Glückwunsch! Du hast', i_versuch, 'Versuche gebraucht!')
  ```

  ```python
  zahl1 = input("Gib mir eine Zahl! ")
  zahl1 = int(zahl1)

  zahl2 = input("Gib mir noch eine Zahl! ")
  zahl2 = int(zahl2)

  counter = int(1)

  while True:
  	print("Was ist die Summe dieser Zahlen?")
  	antwort = input()
  	antwort = int(antwort)

  	if antwort == zahl1 + zahl2:
  		print("Richtig!")
  		print("Versuche: ", counter)
  	else:
  		print("Das war falsch!")
  		counter = counter + 1
  ```

  ```python
  versuch = 0
  aktiv = True
  zahl1 = int(input("Gib mir eine Zahl! "))
  zahl2 = int(input("Gib mir noch eine Zahl! "))
  antwort_richtig = int(zahl1 + zahl2)

  while aktiv:
      antwort = int(input('Was ist die Summe dieser Zahlen? '))    
      versuch = versuch + 1
      if antwort == antwort_richtig:
          print(('\nRichtig! Du hast soviele Versuche gebraucht: '), versuch)
          aktiv = False
      else:
          print('\nFalsch, überleg nochmal!')
  ```       

  ```python

  abgleich = False
  zahl1 = int(input("Gib mir eine Zahl! "))
  zahl2 = int(input("Gib mir noch eine Zahl! "))
  versuche = 0
  loesung = int(zahl1 + zahl2)

  #print(loesung)

  while not abgleich:
  Antwort= int(input("Was ist die Summe dieser Zahlen? "))
  versuche = versuche + 1
  if Antwort == loesung:
    abgleich = True

  else:
    print ("Das war leider falsch. Denk noch einmal scharf nach, dann fällt es dir sicher ein.")

  print("Richtig, das Ergebnis lautet " + str(loesung) +".")
  print ("Du hast " + str(versuche) +" Versuch gebraucht.")
  ```

  ```python
  zahl1 = int(input("Gib mir eine Zahl! "))

  zahl2 = int(input("Gib mir noch eine Zahl! "))

  antwort = int(input("Was ist die Summe dieser Zahlen? "))
  fehler = (zahl1 + zahl2) - antwort

  while fehler:
      print("Leider falsch!")
      versuch = versuch + 1
      print()
      antwort = int(input("Was ist die Summe dieser Zahlen? "))
      fehler = (zahl1 + zahl2) - antwort

  if versuch == 1:
      print("Super, beim ersten Vversuch!")
  else:
      print(f"Richtig! Du hast {versuch} Versuche gebraucht.")
  ```

  ```python
  versuch = 0

  zahl1 = input("Gib mir eine Zahl! ")
  zahl1 = int(zahl1)

  zahl2 = input("Gib mir noch eine Zahl! ")
  zahl2 = int(zahl2)

  print("Was ist die Summe dieser Zahlen? ")

  ergebnis = True

  while ergebnis:
      versuch = versuch + 1
      antwort = input()
      antwort = int(antwort)
      if zahl1 + zahl2 == antwort:
          print("Das ist richtig. Du hast", versuch, "Versuch(e) benötigt.")
          ergebnis = True
          break
      else:
          print("Das ist falsch, probier es noch einmal.")
    ```

    ```python
    versuche = 1
    print()
    zahl1 = input("Gib mir eine Zahl! ")
    zahl1 = int(zahl1)
    zahl2 = input("Gib mir noch eine Zahl! ")
    zahl2 = int(zahl2)
    print("Was ist die Summe dieser Zahlen?")

    antwort = input()
    antwort = int(antwort)
    fehler = (zahl1+zahl2-antwort) #alles außer 0 ist True

    while fehler != 0:
    print ("Falsch!")
    if fehler < 0:
        #print(fehler)
        print ("Zu hoch gegriffen.")
        if fehler < -5:
            #print(fehler)
            print ("Min. 5 zu viel! Viel zu hoch.")
    else:
        print ("Zu niedrig.")
    fehler = (zahl1+zahl2-int(input("Rechne nochmal. Eingabe: ")))
    versuche = versuche + 1
    print ("Richtig! Du hast",versuche, "Versuch(e) gebraucht.")
    #  break
    #   print("Du hattest nun",anzahl_versuche,"Versuche. Das Spiel ist aus.")
    ```

 ```python
  zahl1 = input("Gib mir eine Zahl! ")
  zahl1 = int(zahl1)

  zahl2 = input("Gib mir noch eine Zahl! ")
  zahl2 = int(zahl2)

  print("Was ist die Summe dieser Zahlen?")
  antwort = input()
  antwort = int(antwort)

  versuch = 0

  while antwort != (zahl1 + zahl2):
      print("Das war falsch! " * abs(zahl1 + zahl2 - antwort))
      print("Was ist die Summe dieser Zahlen?")
      antwort = input()
      antwort = int(antwort)
      versuch += 1

  print("Das ist richtig, du hast nur")
  print (versuch)
  print("Versuche gebraucht")
  if versuch <= 0:
      print("Das war sehr gut :o)")
  ```

  ```python
  zahl1 = input("Gib mir eine Zahl! ")
  zahl1 = int(zahl1)

  zahl2 = input("Gib mir noch eine Zahl! ")
  zahl2 = int(zahl2)
  zaehler = 0

  print("Was ist die Summe dieser Zahlen?")
  #antwort = input()
  antwort = -1
  antwort = int(antwort)

  while zahl1 + zahl2 != antwort:
    antwort = int(input("Raten Sie: "))
    if antwort < zahl1 + zahl2:
        print("Zu klein")
    if antwort > zahl1 + zahl2:
        print("Zu groß")

    zaehler = zaehler + 1
  else:
    print("Du es geschafft! " + "Und",zaehler, "antworte gebraucht!")
  ```

  ```python
  zahl1 = int(input("Gib mir eine Zahl! "))
  zahl2 = int(input("Gib mir noch eine Zahl! "))

  count = 1

  while int(input("Was ist die Summe dieser Zahlen? ")) - (zahl1 + zahl2):
    print("Das war falsch! ")
    count = count + 1 #oder count += 1
  print ("Korrekt bei Versuch Nummer:", count)
  ```
</details>

  3. Aufgabe 5<br>
  Zum Quiz folgt eine Musterlösung von Piko ins Protokoll

  4. Thema `break`

  ```python
  counter = 0

  while counter < 10; # Schleifen Kopf
      print("Runde", counter) # ab hier schleifen Körper
      antwort = input("Weiter?")
      if antwort == "Nein":
          break
          print("Breche ab!") # wird nie ausgeführt, weil break vor dem Print steht.
      counter += 1
  ```

    5. turtle<br>
    Erste Schritte mit turtle:

  ```python
  from turtle import *

  shape("turtle")
  right(90);
  forward(50);
  ```


## Pads
Verschiedene Lösungen zu Aufgabe 3.3  
https://md.ha.si/PzIEBH9LQVWG0p0kpAYqUw?view

## Pikos Dateien

Musterlösung Quiz (kann gut sein, dass das nicht die beste Lösung ist – wenn Ihr mit Eurem Code zufrieden seid, dann passt er höchstwarscheinlich auch.)
```python
frage1 = "Was ist der sonnennächste Planet im Sonnensystem? "
antwort1 = "Merkur"
frage2 = "Was bekommt Ihr nächste Woche statt der normalen Stunde vom Python-Kurs? "
antwort2 = "Ein Video"
frage3 = "Was ist die richtige Anzahl an Schafen? "
antwort3 = "Vier"

print("Hallo! Hier sind drei Fragen:")
versuch1 = input(frage1)
if versuch1 == antwort1:
    print("Das ist richtig!")
else: 
    print("Das ist falsch!")

versuch2 = input(frage2)
if versuch2 == antwort2:
    print("Das ist richtig!")
else: 
    print("Das ist falsch!")

versuch3 = input(frage3)
if versuch3 == antwort3:
    print("Das ist richtig!")
else: 
    print("Das ist falsch!")
```

```python
# while + break : Minimalbeispiel
counter = 0

while counter < 10:
    counter += 1
    print("Runde", counter)
    antwort = input("Weiter?")
    if antwort == "Nein":
        print("Breche ab!")
        break
```

```python
from turtle import *

shape("turtle")

forward(100)
right(90)
forward(100)
right(60)
forward(20)
```