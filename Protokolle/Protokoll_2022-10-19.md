# Pikos Python Kurs am 19.10.2022


## Mitschrift
Protokoll 19.10.

### Besprechung der Hausaufgaben
Hinweis zu 0:
Stackoverflow = wichtige Webseite für „Profis“, Bewertungen/Korrekturen zur Frage und zu den Antworten sind mögkich.

Nachbesprechung Aufgabe 2.4, Unterschied beim Verändern von Listen zwischen [2] und [2:3] bzw. [2:2]

Beispiel
liste = [[„eins“, „zwei“, „drei“], [„zwei“, „drei“, „vier“], [„drei“, „vier“, „fünf“]]

>>> liste[1]
['zwei', 'drei', 'vier']

Ausgabe des Elements, Python kuckt nicht tiefer, sagt einfach nur, was an 1. Stelle steht.

>>> liste[2:3]	
[['drei', 'vier', 'fünf']]

Ausgabe wird in eine Liste gepackt

>>> liste[0:2]
[['eins', 'zwei', 'drei'], ['zwei', 'drei', 'vier']]

Macht eine Liste der Listen.

Fazit: Mit Doppelpunkt kommen da Listen raus, ohne Doppelpunkt das, was es ist (ne Liste, ne Tupel, n String…) Doppelpunkte machen slices, also Abschnitte.

Wenn man liste [1:1] macht, ist es ne leere liste

PikosBild:
Wenn wir mit dem Löffel in eine Schüssel salat gehen, holen wir immer Salat damit raus (mit Doppelpunkt)
Mit der Gabel holen wir ein einzelnes Ding raus, ne Tomate oder so (ohne Doppelpunkt)

Geht dann auch andersrum, wenn man Teile ersetzt:
`liste[1] = "drei"`    – setzt „drei an Stelle des Elements 1 der Liste ( Tomate an stelle der Gurke) 

Beispiel aus der Kommandozeile von Piko:
liste[7:8] = [7.2, 7.3, 7.6] hiermit wird ein Abschnitt statt der 7 hineingeschoben (und der Abschnitt [7:8] – das ist die 7 - ersetzt)

Ohne was rauszunehmen etwas in die Liste reintun: 
liste[1:1] – gibt nix zurück, und in dieses nix kann man was reinschieben.

Aufgabe 3: Debugging
In Zeile 6 ist es wichtig, dass der Buchstabe durch Klammern und Komma zur tupel hinzugefügt wird.


### Hausaufgaben fertig. Ab zum Stoff…..

methoden
liste.append(„xxx“) fügt am ende der liste xxx hinzu

Was macht liste.insert?

Beispiel:
liste = ["aaa", "bbb", "ccc"] 
liste.insert(1,"ddd")

print(liste)
['aaa', 'ddd', 'bbb', 'ccc']

Ergo: insert fügt etwas an eine bestimmte Position in eine Liste ein.

Zu Funktionen und Methoden:
Es gibt Funktionen, die objekte verändern und welche, die neue objekte erschaffen.
Beides gibt es sowohl mit Return-Wert, also etwas, mit dem wir weiterarbeiten können, z.B. bei den Mathefunktionen, und ohne Return-Wert.

Pikos Eselsbrücke:
Organgensaftpresse = da tut man oben was rein und am ende kommt unten was raus in den Becher (mit Returnwert)
Solarium = da geht man rein, das verändert einen,  aber da kommt am ende nix raus (ohne Returnwert)

|                  | Methoden           | Funktionen |
|------------------|--------------------|------------|
| Mit return-Wert  | upper()            | ord()      |
| Ohne return-Wert | append(), insert() | fd()       |

append und insert verändern die Welt – in dem Fall, die Liste - und geben kein ergebnis zurück.(Solarium) Dehalb ist in der Ergebnisvariable auch nichts (NONE) drin (Pikos Kommandozeile anschauen)

Methoden haben immer einen Punkt. eine Funktion mit Returnwert ist z.B. ord() und eine ohne return z.b. fd()

Welche Methode macht aus einem string Großbuchstaben?
upper = macht eine Ausgabe in Großbuchstaben

Beispiel:
```
>>> text = "hallo welt"
>>> print(text.upper())
HALLO WELT
```

Wie macht man aus einem String eine liste aus mehreren strings? - split
„My hovercraft is full of eels“

Beispiel:
```
>>> satz = "my hovercraft is full of eels"
>>> print(satz.split())
['my', 'hovercraft', 'is', 'full', 'of', 'eels']
```
Variablen sollten immer mit kleinen Buchstaben anfangen



## Pikos Dateien
```python
wort = "Franzbroetchen"
vokale = []

for buchstabe in wort:
    if buchstabe in "aeiou":
        vokale.append(buchstabe)

print(vokale)


buchstabe = wort[0]
if buchstabe in "aeiou":
    vokale.append(buchstabe)

buchstabe = wort[1]
if buchstabe in "aeiou":
    vokale.append(buchstabe)

buchstabe = wort[2]
if buchstabe in "aeiou":
    vokale.append(buchstabe)
    
buchstabe = wort[3]
if buchstabe in "aeiou":
    vokale.append(buchstabe)
```

```python


personenliste = [("Jana", "Informatik", "Hamburg"),
                 ("Anna", "Biologie", "Karlsruhe"),
                 ("Harald", "Biologie", "Leipzig")]

person1, person2, person3 = personenliste



l = [1, 2, 3]

zahl1, zahl2, zahl3 = l


zahl1, zahl2, zahl3 = [1, 2, 3]
```

```python
liste = ["ananas", "birne", "papaya", "drachenfrucht", "himbeere"]

liste.append("banane")
print(liste)

ergebnis = liste.insert(2, "mango")
print("Ergebnis:", ergebnis)
print("Liste:", liste)
```

```python
ausgangsstring = "My hovercraft is full of eels."
# Ergebnisstring: ['My', 'hovercraft', 'is', 'full', 'of', 'eels.']
ergebnis = ausgangsstring.split()
print(ergebnis)
```

## Pikos Shell
```
...
>>> liste2[5] = ["fuenf", "fünf", 5.0]
>>> liste2
[0, 1, 2, 'drei', 4, ['fuenf', 'fünf', 5.0], 6, 7, 8]
>>> liste2[7:8] = [7.1, 7.3, 7.8]
>>> liste2
[0, 1, 2, 'drei', 4, ['fuenf', 'fünf', 5.0], 6, 7.1, 7.3, 7.8, 8]
>>> 


...


>>> %Run -c $EDITOR_CONTENT
['ananas', 'birne', 'papaya', 'drachenfrucht', 'himbeere', 'banane']
Ergebnis: None
Liste: ['ananas', 'birne', 'mango', 'papaya', 'drachenfrucht', 'himbeere', 'banane']
>>> z = ord("a")
>>> z
97
>>> from turtle import *
>>> a = fd(100)
>>> a
>>> print(a)
None
>>> wort = "baerenklau"
>>> wort.upper()
'BAERENKLAU'
>>> ausgabe = wort.upper()
>>> ausgabe
'BAERENKLAU'
>>> wort
'baerenklau'
>>> 
```


## Pikos Lösungen
... zu den Hausaufgaben von letztem Mal:
