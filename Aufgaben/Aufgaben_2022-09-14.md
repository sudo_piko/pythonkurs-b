# Aufgaben am 14.09.2022

**Am 21.09. fängt der Python-Kurs erst um 20:00 an!**

# 0 - Lesestoff
## Wahrheitstabelle auf Wikipedia:  
https://de.wikipedia.org/wiki/Wahrheitstabelle
(Wen formale Logik mehr interessiert...)

## Unterhaltung
![](https://md.ha.si/uploads/d5198650cfa5b07fadb927dfb.png)

## 1 – Die schreckliche Tabelle

Vergleicht die Tabelle und das tldraw, ergänzt, was Euch noch fehlt. Packt gerne englische Begriffe zu den deutschen
https://www.tldraw.com/r/1662661671451 

(Und OMG ist das schon schön geworden!)

## 2 – Partysnacks 2

Vergleicht das folgende Programm mit dem im Protokoll. 
- Wie unterscheiden sie sich? Warum?
- Was ist in kuchen_ok und in kekse_ok? Warum ist das gut, das da rein zu tun anstatt es so zu machen wie im Protokoll?

```python
n_gaeste = int(input("Wie viele Gäste kommen? "))
n_personen = n_gaeste + 1
n_kuchenstuecke = int(input("Wie viele Kuchenstücke hast du?? ")) # Direkt in integer umwandeln
n_kekse = int(input("Wie viele Kekse hast du? "))

# print(n_kuchenstuecke, n_kekse)

# kompliziertes Zeug auswerten, bevor es in das if geht
kuchen_ok = n_kuchenstuecke % 6 == 0 and n_kuchenstuecke > 0
kekse_ok = n_kekse > 30

# Einmal überprüfen, ob da wirklich das richtige drin ist (wird dann später auskommentiert):
print(kuchen_ok, kekse_ok)

if kuchen_ok or kekse_ok: # genug kekse/kuchen
    print("Die Party kann losgehen!")
else:
    print("Problem entdeckt! Falsche Anzahl an Snacks!")
```


## 3 – if und while
### 3.1 Farben Raten
Baut ein (zugegeben noch recht langweiliges) Ratespiel, in dem eine Farbe erraten werden muss. Das Spiel läuft so lange, bis die richtige Farbe eingegeben wurde. Dann gibt es einen Glückwunsch aus.
<details> 
  <summary>Tipps </summary>
    Das wird recht ähnlich zum Matheübungsprogramm von vorletzter Woche, nur dass die richtige Antwort vorher im Code festgelegt wird und nicht aus zwei Eingaben errechnet wird.

Und natürlich muss alles in eine while-Schleife, die so lange fragt, bis die Farbe richtig erraten wurde. Da könnt Ihr Code aus der Obstschale übernehmen!
</details>

### 3.2 Passwort-Eingabe
Baut eine Passwort-Eingabe, die überprüft, ob das Passwort genau richtig ist und dann die Person begrüßt. Auch hier muss das Passwort vorher im Code festgelegt sein. 
Solange das Passwort nicht richtig eingegeben wurde, soll sie immer wieder fragen.

<details> 
  <summary>Tipps </summary>
    Diese Aufgabe ist sehr ähnlich zu Aufgabe 3.1. 
</details>

### 3.3 – Matheübungsprogramm
Kramt nocheinmal das Matheübungsprogramm raus und gebt den Spielenden mehrere Versuche! 

Schafft Ihr es, am Ende auch noch auszugeben, wie viele Versuche die Spielenden gebraucht haben?
<details> 
  <summary>Tipps </summary>
   Für das Zählen der Versuche braucht Ihr eine Variable, die vor der Schleife, also am Anfang des Spiels, auf 0 gesetzt wird und dann jedesmal um eins erhöht wird – ungefähr so, wie die Obstschale in unserem Programm (siehe Protokoll) jedes Mal um einen Apfel erweitert wird...
</details>

## 4 – Obstschale

In der Stunde kam folgende Frage zum Obstschalenfüllautomaten:
Gibt es einen Weg, das letzte "Habe was reingetan!" wegzubekommen?

Dieses Programm macht das nicht, aber verwendet eine Sache, die das ermöglichen könnte:
```python
# eine while-Schleife: Ein Obstschalen-Füll-Automat, Version 3

passtnochwasrein = True
obstschale = ("apfel", "apfel", "banane", "kokosnuss")

while passtnochwasrein:
    print("Inhalt der Obstschale:", obstschale)
    obstschale = obstschale + ("apfel",)
    print("Habe was reingetan!")
    if len(obstschale) >= 10:
        print("Jetzt ist aber voll!")
        break
    print("Länge der Obstschaleninhaltstupels: ", len(obstschale))
    
print("Hier geht es weiter!")
```

- Findet das Keyword, das in diesem Programm neu ist und vergleicht, was da ersetzt worden ist. 
- Führt die Programmversionen aus und vergleicht, wie sich die Ausgabe unterscheidet. 
- Sucht das Keyword (und das Stichwort Python) im Internet und schaut Euch ein paar Beispiele an. Versucht zu verstehen, was dieses Keyword tut!

## 5 - Quiz
Löst Aufgabe 3 von letzter Woche, falls Ihr es noch nicht getan habt!


