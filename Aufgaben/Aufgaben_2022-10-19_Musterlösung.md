# Musterlösungen zu den Aufgaben am 19.10.2022
Dies sind Lösungsvorschläge. Eure Lösung kann anders aussehen und es ist gut möglich, dass Ihr auf elegantere Lösungen kommt. 
Wichtig ist nur, dass Ihr an ein zufriedenstellendes Ergebnis kommt und den Lösungsweg hier gut verstehen könnt. 

Habt eine schöne Woche!

## 1 – Slicing vs. Indexing
Fünf Artikel aus der Verfassung von Užupis:
```
4. Everyone has the right to make mistakes.
5. Everyone has the right to be unique.
6. Everyone has the right to love.
7. Everyone has the right not to be loved, but not necessarily.
8. Everyone has the right to be undistinguished and unknown.
```
- Teilt die einzelnen Artikel mit split in eine Wort-Liste auf.
```python
artikel4 = "4. Everyone has the right to make mistakes."
artikel5 = "5. Everyone has the right to be unique."
artikel6 = "6. Everyone has the right to love."
artikel7 = "7. Everyone has the right not to be loved, but not necessarily."
artikel8 = "8. Everyone has the right to be undistinguished and unknown."

wortliste4 = artikel4.split()
print(wortliste4)  # Das Ergebnis gleich mal anschauen... 
# => "4." wird auch als Wort gesehen. Je nach Verwendungszweck wollen wir das oder nicht. 
# Bei der Indexing-Aufgabe klärt sich das dann...
```
- Wie könnt Ihr die Wort-Liste so printen lassen, dass sie trotzdem wie ein Satz aussieht?
```python
for wort in wortliste4:
    print(wort, end=" ")  # Jedes Wort printen, danach nur ein Leerzeichen, kein Zeilenumbruch
print()  # Am Ende noch einen Zeilenumbruch dranhängen; das macht print() a
```
### Indexing
- Wie kommt Ihr bei jeder Wortliste an das sechste Wort? (zB "make" im ersten Satz)
```python
# Wenn ich das "jede" per Hand mache, kann ich fünf Wortlisten erzeugen und jeweils dashier machen:
print(wortliste4[6])
# "Sechstes Wort" ist hier unklar formuliert, da hilft aber das Beispiel weiter:
# "make" soll das sechste Wort sein, damit ist in diesem Fall "4." nicht als Wort (oder als nulltes Wort) gezählt.
```
```python
# Wenn ich das "jede" Python machen lassen will, kann ich eine for-Schleife verwenden,
# dann kann mir auch egal sein, wie viele Artikel es sind!

artikel4 = "4. Everyone has the right to make mistakes."
artikel5 = "5. Everyone has the right to be unique."
artikel6 = "6. Everyone has the right to love."
artikel7 = "7. Everyone has the right not to be loved, but not necessarily."
artikel8 = "8. Everyone has the right to be undistinguished and unknown."

wortliste4 = artikel4.split()

verfassung = [artikel4, artikel5, artikel6, artikel7, artikel8]

for artikel in verfassung:
    wortliste = artikel.split()
    print(wortliste[6])
```
- Ersetzt bei jeder Liste per Indexing (wie im Protokoll direkt unter dem Salat-Beispiel) "right" mit "duty" und lasst Euch das ausgeben.
```python
# Auch hier ist eine for-Schleife praktisch:
verfassung = [artikel4, artikel5, artikel6, artikel7, artikel8]

for artikel in verfassung:
    wortliste = artikel.split()
    wortliste[4] = "duty"
    print(wortliste)
#     Hier könnten wir natürlich auch das print wieder in eine for-Schleife tun, so wie wir es oben gemacht haben:
#     for wort in wortliste:
#         print(wort, end=" ") 
```

### Slicing
- Wie kommt Ihr bei jedem Satz an das dritte bis sechste Wort? 
```python
# Hier übernehmen wir am besten ganz viel Code aus der vorigen Aufgabe.
verfassung = [artikel4, artikel5, artikel6, artikel7, artikel8]

for artikel in verfassung:
    wortliste = artikel.split()
    print(wortliste[3:7])
#     Hier könnten wir natürlich auch das print wieder in eine for-Schleife tun, so wie wir es oben gemacht haben:
#     for wort in wortliste[3:7]:
#         print(wort, end=" ") 

```
- Ersetzt bei jeder Liste per Slicing (so ähnlich wie im Protokoll bei dem [7:8]-Beispiel) "has the right" mit "should be able"
```python
verfassung = [artikel4, artikel5, artikel6, artikel7, artikel8]

for artikel in verfassung:
    wortliste = artikel.split()
    wortliste[2:5] = ["should", "be", "able"]
    print(wortliste)
# Und natürlich könnten wir wieder die for-Schleife für eine schönere Ausgabe verwenden.
```

### Sachen einschieben (Slicing)
Slicing geht auch so:
`liste[2:2] = ["Unfug", "Unsinn", "Albernheit", "Blödelei"]`
- Wenn Ihr mit dieser Syntax noch unsicher seid, dann probiert ersteinmal an einer selbstgebauten Liste, was das tut.
```python
# Zum Beispiel so:
liste = [0, 1, 2, 3, 4]
liste[2:2] = ["Unfug", "Unsinn", "Albernheit", "Blödelei"]
print(liste)
```
- Wie könnt ihr mit dieser Syntax die Wortlisten von oben so ändern, dass da immer ein "and solemn duty" eingefügt wird?
Ungefähr so: "4. Everyone has the right and solemn duty to make mistakes."
```python
verfassung = [artikel4, artikel5, artikel6, artikel7, artikel8]

for artikel in verfassung:
    wortliste = artikel.split()
    wortliste[5:5] = ["and", "solemn", "duty"]
    print(wortliste)
```


## 2 – Entpacken bei der Zuweisung 
### 2.1 Zuweisung in zwei Schritten
```
infos0 = ("Estland", "Tallinn")
land = infos0[0]
hauptstadt = infos0[1]
```

```
Hopsa, hier fehlte die Aufgabenstellung! Was ist in land, was ist in Hauptstadt?
Wichtig ist hier, dass Ihr versteht, was passiert: 
In land wird das gespeichert, was in der Liste infos0 an nullter Stelle ist.
In hauptstadt das, was an Stelle 1 ist. Viel mehr ist da auch nicht dran; aber es ist die Basis für die nächsten Aufgaben...
```

### 2.2 Zuweisung mit Entpacken
Eine Übungsaufgabe für die Leute, die letzte Woche Schwierigkeiten mit der Personenliste hatten.
```
infos0 = ("Estland", "Tallinn")
land, hauptstadt = infos0
```
- Findet heraus, was jetzt in land und hauptstadt ist. Schaut dann den Code nochmal genau an und überlegt, warum das besonders elegant ist.
```
Auch hier ist in land "Estland" und in hauptstadt "Tallinn"
```
- Könnt Ihr damit dann folgenden Satz ausgeben lassen: "Die Haupstadt von Estland ist Tallinn."
```python
print("Die Hauptstadt von " + land + " ist " + hauptstadt + ".")
```
- Macht das Gleiche auch für die beiden anderen baltischen Staaten:
```
infos1 = ("Lettland", "Riga")
infos2 = ("Litauen", "Vilnius")

print("Die Hauptstadt von " + infos1[0] + " ist " + infos1[1] + ".")
print("Die Hauptstadt von " + infos2[0] + " ist " + infos2[1] + ".")
```
- Wenn ich jetzt die Infos nicht als einzelne Tupel habe, sondern als ganze Liste: `infos = ["Estland", "Tallinn"), ("Lettland", "Riga"), ("Litauen", "Vilnius")]`, dann kann ich da mit einer for-Schleife durchgehen:
```python
for info in infos:
    print(info[0], "hat", info[1])
```
**Hinweis**: In der Liste `infos` hat sich ein Tippfehler eingeschlichen: Ich habe die erste runde Klammer übersehen.
Was macht der Code? 
Antwort: Er geht durch die Liste `infos` und gibt für jedes Tupel die erste Stelle und die zweite Stelle aus, und schreibt dazwischen "hat".
Verändert ihn so, dass er das Entpacken mit `land, hauptstadt` verwendet!
```python
infos = [("Estland", "Tallinn"), ("Lettland", "Riga"), ("Litauen", "Vilnius")]
for info in infos:
    land, hauptstadt = info
    print(land, "hat", hauptstadt)
```
### 2.3 Zuweisung in einer for-Schleife
```
lieblingsschokolade = [("Miri", "Weiße Schokolade"), ("Nora", "Kaffeesplitter"), ("Otto", "Vollmilch"), ("Piko", "Bitterschokolade")]
```
Baut eine for-Schleife, die jeweils einen ganzen Satz ausgibt: "Die Lieblingsschokolade von Miri ist Weiße Schokolade. ..."

```python
lieblingsschokolade = [("Miri", "Weiße Schokolade"), ("Nora", "Kaffeesplitter"), ("Otto", "Vollmilch"), ("Piko", "Bitterschokolade")]
for praeferenz in lieblingsschokolade:
    name, schokolade = praeferenz
    print("Die Lieblingsschokolade von " + name + " ist " + schokolade + ".")
```


## 3 – Quiz-Aufgaben-Stückelung
### 3.1 Skelett
Das Quiz ist schon ein etwas größeres Projekt, da ist es gut, sich vorher einmal zu überlegen, was so passieren soll:
- Zuerst kommt der Anfang; vielleicht eine Begrüßung, ein Namen-Eingeben, die Liste der Fragen und Antworten, die Punkte auf 0 setzen (damit sie später hochgezählt und am Ende ausgegeben werden können)
```python
print("Willkommen beim Quiz!")
name = input("Wie heißt du? ")
punkte = 0
fragen = [("Was ist der sonnennächste Planet im Sonnensystem?", "Merkur"), ("Was ist die Hauptstadt von Estland?", "Tallinn"), ("Wie heißt der größte Planet im Sonnensystem?", "Jupiter")]
```
- Dann kommt die for-Schleife mit den einzelnen Fragen, die in Aufgabe 3.2 genauer betrachtet wird... 
```python
# Da erstmal das Skelett:
for dings in fragen:
    print("FRAGEEEEEEE")
    punkte = punkte + 1
```

- Dann am Ende kommt die Ausgabe des Punktestandes. 
```python
# Da wird dann punkte eine Zahl sein, genauer die Anzahl aller gegeben richtigen Antworten; fragen wird sich nicht verändert haben. len(fragen) ist dann die Anzahl aller Fragen.
print("Du hast", punkte, "von", len(fragen), "Fragen richtig beantwortet,", name + ".")
```
Es ist sinnvoll, sowas erstmal so weit zu bauen – auch wenn die Schleife vielleicht nur "FRAGE1, FRAGE2, FRAGE3" printet und am Ende der Punktestand immer 100% ist...
Diese Vorgehensweise heißt "top-down", weil eins von weit oben/weit weg, sozusagen aus der Vogelperspektive, immer näher ranzoomt.  

### 3.2 Die Schleife 
Wenn Euch der Anfang und das Ende gefallen, könnt Ihr an der for-Schleife basteln. Was in der For-Schleife steht, ist sozusagen eine Runde des Quiz, wird also in jeder Runde ausgeführt. 
Stellt Euch vor, Ihr spielt dieses Quiz als Brettspiel; welche Dinge passieren wann? Schreibt das auf Deutsch auf! Und zwar ziemlich detailliert, so in etwa:  
"Die Quizmeisterin liest die erste Frage vor. Der\*die Quizspieler\*in überlegt. Ersie gibt eine Antwort." und so weiter. 

Text-Antwort: Die Quizmeisterin liest die erste Frage vor. Der\*die Quizspieler\*in überlegt. Ersie gibt eine Antwort. Die Quizmeisterin überprüft, ob die Antwort richtig ist, und teilt mit, ob sie richtig ist. Wenn ja, bekommt der\*die Quizspieler\*in einen Punkt. Wenn nein, bekommt er\*sie keinen Punkt. Die Quizmeisterin liest die zweite Frage vor. [...]

Oder als Code:
```python
for frage, loesung in fragen:
    gegebene_antwort = input(frage)
    if gegebene_antwort == loesung:
        print("Richtig!")
        punkte += 1
    else:
        print("Das war leider falsch!")
        print("Die richtige Antwort ist:", loesung)
```

Das vollständige Quiz-Programm:

```python
print("Willkommen beim Quiz!")
name = input("Wie heißt du? ")
punkte = 0
fragen = [("Was ist der sonnennächste Planet im Sonnensystem?", "Merkur"), ("Was ist die Hauptstadt von Estland?", "Tallinn"), ("Wie heißt der größte Planet im Sonnensystem?", "Jupiter")]




for frage, loesung in fragen:
    gegebene_antwort = input(frage)
    if gegebene_antwort == loesung:
        print("Richtig!")
        punkte += 1
    else:
        print("Das war leider falsch!")
        print("Die richtige Antwort ist:", loesung)
print("Du hast", punkte, "von", len(fragen), "Fragen richtig beantwortet,", name + ".")
```

## 4 – Funktionen mit vs. ohne return-Wert
Schaut auf dem tl;draw die Funktionen durch und ordnet sie jeweils zu: Welche Funktionen haben einen Return-Wert (Orangensaft) und welche haben keinen (Solarium).  
https://www.tldraw.com/r/1662661671451

Antwort: Die allermeisten Funktionen haben einen Return-Wert.  
Funktionen ohne Return-Wert: 
- print(); da besteht die "Weltveränderung" aus der Ausgabe, 
- viele von den Turtle-Funktionen; da besteht die Weltveränderung aus der Zeichenbewegung/Veränderung der Turtle
- reverse() und sort() (siehe unten...)

## 5 – Methoden
### 5.1 Listen-Methoden
Folgende Methoden sind bei Listen immer sehr nützlich:  
pop, reverse, index, sort

Findet heraus, was sie tun und überlegt Euch zu einer davon eine kurze Aufgabe, die Ihr den anderen in Eurer Gruppe stellt. 
Teilt die Aufgaben hier, wenn Ihr wollt:
https://md.ha.si/-thQgwB3SJ-_2EaYdM1wgg#
```
Vielen Dank! Das sind spannende, und teils ganz schön knifflige Aufgaben. Werft da gerne einen Blick rein und schaut mal, ob Ihr eine davon lösen könnt!
```
Was die Methoden tun:

- `pop()`: Entfernt das letzte Element aus der Liste (die Liste wird verändert, Solarium) und gibt es als Return-Wert zurück (Orangensaft). Wir haben hier also sowohl eine verändernde Funktion, als auch eine Funktion mit Return-Wert... Das gibt es auch...
- `reverse()`: Dreht die Reihenfolge der Elemente in der Liste um. Verändert die Liste (Solarium) und hat keinen Return-Wert.
- `index()`: Gibt den Index des ersten Elements als Return-Wert zurück, das dem Argument entspricht (Orangensaft).
- `sort()`: Sortiert die Liste. Verändert die Liste (Solarium) und hat keinen Return-Wert.

### 5.2 String-Methoden
- split() hat auch die Möglichkeit, ein Argument mitzugeben (also etwas in die Klammern zu schreiben). Bevor Ihr experimentiert: Überlegt, was da wohl sinnvoll wäre. Experimentiert dann und schlagt im Netz nach.
  - **Antwort**: `split()` teilt einen String anhand eines Trennzeichens in eine Liste von Strings auf. Das Trennzeichen wird in den Klammern übergeben, und kann jedes Zeichen oder Zeichengruppe (also zB auch "und") sein.
- Schlagt im Netz nach, was split noch so für Argumente annehmen kann. Was macht das? Wofür ist das nützlich?
  - **Antwort**: Programiz hat hier eine gute Erklärung; schaut Euch auch die Beispiele an!: https://www.programiz.com/python-programming/methods/string/split
- Was tut isalpha()? Was für einen Datentyp gibt das zurück? Wofür könnte eins das verwenden?
  - `isalpha()` überprüft, ob ein String nur aus Buchstaben besteht. Wenn ja, dann gibt es `True` zurück, wenn nein, dann `False`. Das lässt sich zB für `if` ganz gut verwenden:
```python
geheimes_passwort = input("Gib ein neues Passwort ein: ")
if geheimes_passwort.isalpha():
    print("Dein Passwort ist zu einfach. Verwende Zahlen und Sonderzeichen!")
```

## 6 – Hangman
Spielt erstmal Hangman, um Euch an die Regeln zu erinnern: https://hangmanwordgame.com/?fca=1&success=0#/
Sagen wir, ich programmiere ein Hangman-Spiel. 
Ich habe den Buchstaben-Rateversuch "e" per `input()` bekommen und in der Variable `buchstabe` gespeichert.
Er kommt im Lösungswort `loesung = "ueberschallknall"`mehrfach vor. (Käme er nur einmal vor, könnte ich es mit der Methode index() machen, aber das klappt bei mehrfachen Vorkommnissen leider nicht...)
Wir brauchen eine Liste mit den Indizes des Buchstabens, also in diesem Fall [1, 3]. Wie kann ich die bauen?

Antwort:

```python
indizes = []
for i in range(len(loesung)):
    if loesung[i] == buchstabe:
        indizes.append(i)
        print(indizes)  # Dann können wir den Indizes beim Wachsen zuschauen!
```


<details> 
  <summary>Tipps </summary>
   Zuerst könnt Ihr Euch Euer Vorgehen überlegen: Eigentlich würde eins erwarten, dass die Methode index() sowas kann. Leider sagt eine kurze Internet-Suche, dass das so leider nicht geht. Deshalb machen wir es ohne index(): 
Ihr müsst 1. durch alle Buchstaben von "ueberschallknall" durchlaufen, 2. jeden dieser Buchstaben mit dem Rateversuch vergleichen. 3. Immer, wenn sie übereinstimmen, sie in eine Liste packen.
</details>

<details> 
  <summary>Tipps </summary>
Ihr braucht eine for-Schleife, mit der Ihr eigentlich durch die einzelnen Buchstaben laufen wollt. Weil Euch aber _gleichzeitig_ wichtig ist, einen integer zu haben, an welcher Stelle Ihr seid, müsst Ihr mit der for-Schleife durch range(len(loesung)) laufen.
</details>


<details> 
  <summary>range(len(loesung)) erklärt </summary>
   Bevor Ihr weiterlest, probiert folgendes aus (natürlich mit Einrückungen, die gehen hier in den Tipps leider nicht...): for i in range(len(12)): print(i) # Wenn Ihr das verstanden habt, wie kommen wir dann an den nullten, ersten, zweiten, usw. Buchstaben?
</details>


<details> 
  <summary>Tipps </summary>
   "for i in range(len(12)): print(i)" gibt uns die Zahlen von 0 bis 11. Wenn wir an die Buchstaben Nummer 0 bis 11 drankommen, dann müssen wir von dem Lösungswort jeweils den nullten, ersten, etc, Buchstaben indizieren. Das machen wir mit loesung[xxx]. 
    In das xxx muss im ersten Schleifendurchlauf eine 0, in der zweiten eine 1, in der dritten eine 2, usw – das ist ja dann die Aufgabe der for-Schleife... Der Rest der Aufgabe ist leichter; vielleicht schafft Ihr es jetzt alleine weiter?
</details>


<details> 
  <summary>Tipps </summary>
   Dann müsst Ihr prüfen, ob der Buchstabe vorkommt. Das ist ein ganz normales if; Ihr könnt Euch dafür von älteren Codeschnipseln inspirieren lassen. Und diese Buchstaben müssen zusammen in eine Liste – die muss vorher schon existieren; also ganz am Anfang definiert werden.
</details>



Bedenkt, wir wollen kein fertiges Programm, nur die Liste aller Vorkommen des geratenen Buchstabens (falls er überhaupt vorkommt...)